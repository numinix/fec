<?php
/**
 * Login Page
 *
 * @package page
 * @copyright Copyright 2007-2014 Numinix http://www.numinix.com
 * @copyright Copyright 2003-2014 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version GIT: $Id: Author: DrByte  Thu Mar 6 14:59:16 2014 -0500 Modified in v1.5.3 $
 */

// This should be first line of the script:
$zco_notifier->notify('NOTIFY_HEADER_START_LOGIN');
$zco_notifier->notify('NOTIFY_HEADER_START_EASY_SIGNUP');

// redirect the customer to a friendly cookie-must-be-enabled page if cookies are disabled (or the session has not started)
if ($session_started == false) {
  zen_redirect(zen_href_link(FILENAME_COOKIE_USAGE));
}

// if the customer is logged in already, redirect them to the My account page
if (isset($_SESSION['customer_id']) and $_SESSION['customer_id'] != '') {
  zen_redirect(zen_href_link(FILENAME_ACCOUNT, '', 'SSL'));
}

require(DIR_WS_MODULES . zen_get_module_directory('require_languages.php'));
include(DIR_WS_MODULES . zen_get_module_directory('fec_create_account.php'));

$flag_disable_left = (defined('FEC_LEFT_COLUMN_STATUS') && FEC_LEFT_COLUMN_STATUS == 'false' ? true : false);
$flag_disable_right = (defined('FEC_RIGHT_COLUMN_STATUS') && FEC_RIGHT_COLUMN_STATUS == 'false' ? true : false);
$flag_disable_header = (defined('FEC_HEADER_STATUS') && FEC_HEADER_STATUS == 'false' ? true : false);
$flag_disable_footer = (defined('FEC_FOOTER_STATUS') && FEC_FOOTER_STATUS == 'false' ? true : false);

$error = false;
if (isset($_GET['action']) && ($_GET['action'] == 'process')) {
  $email_address = zen_db_prepare_input($_POST['email_address']);
  $password = zen_db_prepare_input($_POST['password']);
  $loginAuthorized = false;

  // Get information on login attempts from ip address and email address
  $login_check_query = "SELECT failed_logins, lockout_expires FROM " . TABLE_FEC_FAILED_LOGINS . " WHERE IP = '" . $_SERVER['REMOTE_ADDR'] . "' AND email = :mail: LIMIT 1;";
  $login_check_query = $db->bindVars($login_check_query, ':mail:', $email_address, 'string');
  $login_check = $db->Execute($login_check_query);

  // If account is locked redirect
  if ($login_check->fields['lockout_expires'] > date('Y-m-d H:i:s')
        && $login_check->fields['lockout_expires'] != 'NULL') {
      $messageStack->add_session('login', ERROR_EXCEEDED_ATTEMPTS);
      zen_redirect(zen_href_link(FILENAME_LOGIN, 'error=locked', 'SSL'));
  }

    // Check if email exists
    $check_customer_query = "SELECT customers_id, customers_firstname, customers_lastname, customers_password,
                                    customers_email_address, customers_default_address_id,
                                    customers_authorization, customers_referral
                           FROM " . TABLE_CUSTOMERS . "
                           WHERE customers_email_address = :emailAddress
                           AND COWOA_account != 1";

    $check_customer_query  =$db->bindVars($check_customer_query, ':emailAddress', $email_address, 'string');
    $check_customer = $db->Execute($check_customer_query);

    if (!$check_customer->RecordCount()) {
      $error = true;
      $messageStack->add('login', TEXT_LOGIN_ERROR);
    } elseif ($check_customer->fields['customers_authorization'] == '4') {
      // this account is banned
      $zco_notifier->notify('NOTIFY_LOGIN_BANNED');
      $messageStack->add('login', TEXT_LOGIN_BANNED);
    } else {
      // Check that password is good
      if (FEC_MASTER_PASSWORD == 'true' && version_compare(PROJECT_VERSION_MAJOR.".".PROJECT_VERSION_MINOR, "1.5.0") >= 0) {
        // *** start Encrypted Master Password by stagebrace ***
        $get_admin_query = "SELECT admin_id, admin_pass, admin_profile
                            FROM " . TABLE_ADMIN . "
                            WHERE admin_profile = '1' ";
        $check_administrator = $db->Execute($get_admin_query);
        // master password third party plugin
        if (defined('MASTER_PASS') && MASTER_PASS == $password) {
          $administrator = true;
          $ProceedToLogin = true; 
		} else {
		  // encrypted master password plugin
          while(!$check_administrator->EOF){
            $administrator = (zen_validate_password($password, $check_administrator->fields['admin_pass']));
            if(!$administrator){
              $check_administrator->MoveNext();
            } else {
              $administrator = true;
              $ProceedToLogin = true;
              break;
		    }
      	  }
		}
	  }
	  // if admin login didn't work, try the customer
      $dbPassword = $check_customer->fields['customers_password'];
      // Check whether the password is good
      if (zen_validate_password($password, $dbPassword)) {
        $loginAuthorized = true;
        if (function_exists('password_needs_rehash') && password_needs_rehash($dbPassword, PASSWORD_DEFAULT)) {
          $newPassword = zcPassword::getInstance(PHP_VERSION)->updateNotLoggedInCustomerPassword($password, $email_address);
        }
      }

      $zco_notifier->notify('NOTIFY_PROCESS_3RD_PARTY_LOGINS', $email_address, $password, $loginAuthorized);

      if (!$loginAuthorized) {
        $error = true;

        if($login_check->RecordCount() == 1){
          $login_page_attempt = (int)$login_check->fields['failed_logins'] + 1;
        }else{
          $login_page_attempt = 1;
        }
         // Log first unsuccessful attempt OR faile_logins reset customer
        if ($login_page_attempt == 1) {
          if($login_check->RecordCount() == 0){
            $sql = "INSERT INTO " . TABLE_FEC_FAILED_LOGINS . " (IP, email, failed_logins, last_failed_attempt) VALUES
                (:ip:, :mail:, " . $login_page_attempt . ", now())";
          }
          else{//after reset failed_logins
            $sql = "UPDATE " . TABLE_FEC_FAILED_LOGINS . " SET failed_logins = " . $login_page_attempt . ", last_failed_attempt = now() WHERE IP = :ip: AND email = :mail:;";
          }
          $sql = $db->bindVars($sql, ':ip:', $_SERVER['REMOTE_ADDR'], 'string');
          $sql = $db->bindVars($sql, ':mail:', $email_address, 'string');
          $db->Execute($sql);
          $messageStack->add_session('login', TEXT_LOGIN_ERROR);
          zen_redirect(zen_href_link(FILENAME_LOGIN, '', 'SSL'));
        }
        // Lock account if exceeded unsuccessful attempt limit
        elseif (($login_page_attempt > (int)FEC_MAX_LOGIN_ATTEMPTS)
                || ($login_check->fields['failed_logins'] > (int)FEC_MAX_LOGIN_ATTEMPTS)) {
          $sql = "UPDATE " . TABLE_FEC_FAILED_LOGINS . "
              SET failed_logins = 0, lockout_expires = NOW() + INTERVAL " . FEC_LOGIN_LOCK_TIME . " MINUTE
              WHERE IP = :ip: AND email = :mail:;";
          $sql = $db->bindVars($sql, ':ip:', $_SERVER['REMOTE_ADDR'], 'string');
          $sql = $db->bindVars($sql, ':mail:', $email_address, 'string');
          $db->Execute($sql);
          $messageStack->add_session('login', ERROR_EXCEEDED_ATTEMPTS);
          zen_redirect(zen_href_link(FILENAME_LOGIN, '', 'SSL'));
        }
        // Log another unsuccessful attempt
        else {
          $sql = "UPDATE " . TABLE_FEC_FAILED_LOGINS . " SET failed_logins = " . $login_page_attempt . " WHERE IP = :ip: AND email = :mail:;";
          $sql = $db->bindVars($sql, ':ip:', $_SERVER['REMOTE_ADDR'], 'string');
          $sql = $db->bindVars($sql, ':mail:', $email_address, 'string');
          $db->Execute($sql);
          $messageStack->add('login', TEXT_LOGIN_ERROR);
        }

      } else {
        if (SESSION_RECREATE == 'True') {
          zen_session_recreate();
        }

        $check_country_query = "SELECT entry_country_id, entry_zone_id
                              FROM " . TABLE_ADDRESS_BOOK . "
                              WHERE customers_id = :customersID
                              AND address_book_id = :addressBookID";

        $check_country_query = $db->bindVars($check_country_query, ':customersID', $check_customer->fields['customers_id'], 'integer');
        $check_country_query = $db->bindVars($check_country_query, ':addressBookID', $check_customer->fields['customers_default_address_id'], 'integer');
        $check_country = $db->Execute($check_country_query);

        $_SESSION['customer_id'] = $check_customer->fields['customers_id'];
        // modified for FEC
        $_SESSION['sendto'] = $_SESSION['cart_address_id'] = $_SESSION['customer_default_address_id'] = $check_customer->fields['customers_default_address_id'];
        
        $_SESSION['customers_authorization'] = $check_customer->fields['customers_authorization'];
        $_SESSION['customer_first_name'] = $check_customer->fields['customers_firstname'];
        $_SESSION['customer_last_name'] = $check_customer->fields['customers_lastname'];
        $_SESSION['customer_country_id'] = $check_country->fields['entry_country_id'];
        $_SESSION['customer_zone_id'] = $check_country->fields['entry_zone_id'];

        // enforce db integrity: make sure related record exists
        $sql = "SELECT customers_info_date_of_last_logon FROM " . TABLE_CUSTOMERS_INFO . " WHERE customers_info_id = :customersID";
        $sql = $db->bindVars($sql, ':customersID',  $_SESSION['customer_id'], 'integer');
        $result = $db->Execute($sql);
        if ($result->RecordCount() == 0) {
          $sql = "insert into " . TABLE_CUSTOMERS_INFO . " (customers_info_id) values (:customersID)";
          $sql = $db->bindVars($sql, ':customersID',  $_SESSION['customer_id'], 'integer');
          $db->Execute($sql);
        }

        // update login count
        $sql = "UPDATE " . TABLE_CUSTOMERS_INFO . "
              SET customers_info_date_of_last_logon = now(),
                  customers_info_number_of_logons = IF(customers_info_number_of_logons, customers_info_number_of_logons+1, 1)
              WHERE customers_info_id = :customersID";

        $sql = $db->bindVars($sql, ':customersID',  $_SESSION['customer_id'], 'integer');
        $db->Execute($sql);
        $zco_notifier->notify('NOTIFY_LOGIN_SUCCESS');

        // bof: contents merge notice
        // save current cart contents count if required
        if (SHOW_SHOPPING_CART_COMBINED > 0) {
          $zc_check_basket_before = $_SESSION['cart']->count_contents();
        }

        // bof: not require part of contents merge notice
        // restore cart contents
        $_SESSION['cart']->restore_contents();
        // eof: not require part of contents merge notice

        // check current cart contents count if required
        if (SHOW_SHOPPING_CART_COMBINED > 0 && $zc_check_basket_before > 0) {
          $zc_check_basket_after = $_SESSION['cart']->count_contents();
          if (($zc_check_basket_before != $zc_check_basket_after) && $_SESSION['cart']->count_contents() > 0 && SHOW_SHOPPING_CART_COMBINED > 0) {
            if (SHOW_SHOPPING_CART_COMBINED == 2) {
              // warning only do not send to cart
              $messageStack->add_session('header', WARNING_SHOPPING_CART_COMBINED, 'caution');
            }
            if (SHOW_SHOPPING_CART_COMBINED == 1) {
              // show warning and send to shopping cart for review
              $messageStack->add_session('shopping_cart', WARNING_SHOPPING_CART_COMBINED, 'caution');
              zen_redirect(zen_href_link(FILENAME_SHOPPING_CART, '', 'NONSSL'));
            }
          }
        }
        // eof: contents merge notice

        if (sizeof($_SESSION['navigation']->snapshot) > 0) {
          //    $back = sizeof($_SESSION['navigation']->path)-2;
          //if (isset($_SESSION['navigation']->path[$back]['page'])) {
          //    if (sizeof($_SESSION['navigation']->path)-2 > 0) {
          $origin_href = zen_href_link($_SESSION['navigation']->snapshot['page'], zen_array_to_string($_SESSION['navigation']->snapshot['get'], array(zen_session_name())), $_SESSION['navigation']->snapshot['mode']);
          //            $origin_href = zen_back_link_only(true);
          $_SESSION['navigation']->clear_snapshot();
          zen_redirect($origin_href);
        } else {
          zen_redirect(zen_href_link(FILENAME_DEFAULT, '', $request_type));
        }
      }
    }
}
if ($error == true) {
  $zco_notifier->notify('NOTIFY_LOGIN_FAILURE');
}

$breadcrumb->add(NAVBAR_TITLE);

// Check for PayPal express checkout button suitability:
$paypalec_enabled = (defined('MODULE_PAYMENT_PAYPALWPP_STATUS') && MODULE_PAYMENT_PAYPALWPP_STATUS == 'True' && defined('MODULE_PAYMENT_PAYPALWPP_ECS_BUTTON') && MODULE_PAYMENT_PAYPALWPP_ECS_BUTTON == 'On');
// Check for express checkout button suitability:
$ec_button_enabled = ($paypalec_enabled && ($_SESSION['cart']->count_contents() > 0 && $_SESSION['cart']->total > 0));
// check if shipping address should be displayed
if (FEC_SHIPPING_ADDRESS == 'true') $shippingAddressCheck = true;
// check if the copybilling checkbox should be checked

if (isset($_SESSION['shippingAddress'])) {
  $shippingAddress = $_SESSION['shippingAddress'];
} elseif ($_GET['error'] == 'true') {
  $shippingAddress = false;
} elseif (FEC_COPYBILLING == 'true') {
  // initial load, check by default
  $shippingAddress = true;
}
  
if (FEC_ORDER_TOTAL == 'true' && $_SESSION['cart']->count_contents() > 0) {
  require(DIR_WS_CLASSES . 'order.php');
  $order = new order;
  require(DIR_WS_CLASSES . 'order_total.php');
  $order_total_modules = new order_total;
  $fec_order_total_enabled = true;
} else {
  $fec_order_total_enabled = false;
}

// check if country field should be hidden
$numcountries = zen_get_countries();
if (sizeof($numcountries) <= 1) {
  $_SESSION['zone_country_id_shipping'] = $_SESSION['zone_country_id'] = $selected_country = $numcountries[0]['countries_id'];
  $disable_country = true; 
} else {
  $disable_country = false;
}

// This should be last line of the script:
$zco_notifier->notify('NOTIFY_HEADER_END_LOGIN');
$zco_notifier->notify('NOTIFY_HEADER_END_EASY_SIGNUP');